#!/usr/bin/env python
from libpitt import exe, path_join
import os
import re
pattern="^(\d )*\w+ 1 *$"
text=""

exe("rm -rf ./books/*")

with open ("msv-reference.txt") as msv:
    text=msv.readlines()
lnum=0
current_title=None
books={}
for line in text:
    if re.match(pattern, line):
        current_title=line.strip()[:-2]
        book_text=[]
        start_line=lnum
        book_text.append(line)
        if current_title:
            book={}
            book["title"]=current_title
            book["start_line"]=start_line
            book["text"]=book_text
            books[book["title"]]=book
    elif current_title:
        book_text.append(line)
    lnum+=1
book={}
book["title"]=current_title
book["start_line"]=start_line
book["text"]=book_text
books[book["title"]]=book
for title, ds in books.iteritems():
    print ds["title"]+" "+str(ds["start_line"])


for title, ds in books.iteritems():
    os.mkdir("./books/"+ds["title"])
    with open("./books/"+ds["title"]+"/start", "w") as start:
        start.write(str(ds["start_line"]))
    with open ("./books/"+ds["title"]+"/book", "w") as book_file:
        book_file.write("\n".join([x.strip() for x in ds["text"]]))
