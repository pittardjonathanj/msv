#!/usr/bin/python
import sys
import re
import os
pattern = "^\d \w+ \d+"
for book in os.listdir("./books"):
    text=""
    with open("./books/"+book+"/book") as book_ref:
        book_list=book_ref.readlines()
    line_count=0
    while line_count<len(book_list):
        line=book_list[line_count]
        if not re.match(pattern ,line):
            line=line.lstrip("01234567890")
        if len(line.strip())>0 and line_count<len(book_list)-1 and not book_list[line_count+1].startswith("["):
            line=line.rstrip()
            text+=line+" "
        else:
            text+=line+"\n"
        line_count+=1
    with open("./books/"+book+"/reader", "w") as dest:
        dest.write(text)
