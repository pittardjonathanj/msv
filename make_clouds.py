#!/usr/bin/env python
from libpitt import exe, path_join
import os
import re
from  wordcloud import WordCloud, STOPWORDS
from matplotlib import cm, font_manager
from random import randint

STOPWORDS.add("thing")
STOPWORDS.add("will")
STOPWORDS.add("things")
fonts = font_manager.findSystemFonts(fontpaths=None)
if not os.path.exists("./clouds"):
    os.mkdir("./clouds")

def get_params():
    to_return={"height": 1080, "width": 1920, "collocations": False, "regexp": "[a-zA-Z]{2,}"}
    cmaps=cm.cmap_d.keys()
    cm_index=randint(0, len(cmaps)-1)
    cmap=cmaps[cm_index]
    bgindex=randint(0, 1)
    if bgindex==0:
        bgcolor="black"
    else:
        bgcolor="white"
    font_index=randint(0,len(fonts)-1)
    font=fonts[font_index]
    to_return["background_color"]=bgcolor
    to_return["colormap"]=cmap
    to_return["font_path"]=font
    print font
    return to_return

text=""
with open ("msv-reference.txt") as msv:
    text=msv.read()
wc=WordCloud(**get_params())
wc.generate(text)
wc.to_file("./clouds/Bible.png")
for book_dir in os.listdir("./books"):
    print book_dir
    with open("./books/"+book_dir+"/book") as book:
        text=book.read()
    wc=WordCloud(**get_params())
    wc.generate(text)
    wc.to_file("./clouds/"+book_dir+".png")
